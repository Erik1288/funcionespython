# funcion que calcula el valor total de una factura, tras aplicarle el iva
def calculateInvoiceTax(iva=21):
    """
    funcion que calcula el total de una factura, recibe como parametro el valor del iva a aplicar
    sino recibe nada, el programa aplicara automaticamente un iva del 21%
    """

    # variable que guardara la cantidad de productos de la factura
    cont = 0

    listNameProducts = []
    listProductQuantity = []
    listValueProduct = []

    while True:
        """
        ciclo infinito que pedira los datos de cada uno de los productos
        a el usuario
        """

        nameProduct = input('Digite el nombre del producto:')
        listNameProducts.append(nameProduct)

        valueProduct = int(input('Digite el valor del producto:'))
        listValueProduct.append(valueProduct)

        quantityProduct = int(input('Digite la cantidad del producto:'))
        listProductQuantity.append(quantityProduct)

        # actualizando la variable contadora en +1, por cada iteracion
        cont += 1

        # variable donde se guardara la decision de agregar otro producto o no
        _option2 = input(
            'Desea ingresar otro producto?, digite(y) para si, o (n) para no: ').lower()

        if _option2 == 'n':

            for i in range(cont):
                print('producto:', listNameProducts[i], ',',
                      'valor:', listValueProduct[i],
                      'cantidad:', listProductQuantity[i])

            # TotalPay: variable donde se guardara el total a pagar
            TotalPay = 0
            for i in range(cont):
                # formula para calcular el iva a una cifra
                TotalPay = TotalPay + \
                    (listValueProduct[i] * listProductQuantity[i])
            # imprimiendo el total a pagar
            print('Total a pagar: ', TotalPay + (TotalPay * iva / 100))

            # rompiendo el ciclo infinito
            break


_option = input(
    'Deseas especificar el valor del iva?, digite (y) para si, o (n) para no: ').lower()
if _option == 'y':
    iva = float(input('ok..., entonces digita el valor del iva: '))
# invocando la funcion, pasandole como parametro el iva definido por el usuario
    calculateInvoiceTax(iva)

else:
    # invocando la funcion sin parametros, en caso de que el usuario no defina el iva
    calculateInvoiceTax()
