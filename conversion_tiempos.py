from nis import match

import math


def ex_2_4(number):
    day = 86400
    hour = 3600
    minute = 60

    secondsDays = number / day
    (DecimalNum, WholeNumber) = math.modf(secondsDays)

    varOne = WholeNumber * day
    varTwo = number - varOne

    secondsHours = varTwo / hour
    (DecimalNumHours, WholeNumberHours) = math.modf(secondsHours)

    varthree = WholeNumberHours * hour
    varfour = varTwo - varthree

    secondsMinutes = varfour / minute
    (DecimalNumMinute, WholeNumberMinute) = math.modf(secondsMinutes)

    varFive = WholeNumberMinute * minute
    secondsSeconds = varfour - varFive

    print(number, 'segundos son:', int(WholeNumber),
          'dias', int(WholeNumberHours), 'horas', int(secondsMinutes), 'minutos y', int(secondsSeconds), 'segundos')


num = int(input('Digite la cantidad de segundos a convertir: '))
ex_2_4(num)
