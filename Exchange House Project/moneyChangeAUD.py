"""
Funcion encargada de converetir dolar australino a dolares EEUU, libras esterlinas, 
moneda colombiana y euros
   
USD: dolar EEUU,
EUR: euro,
GBP: libras esterlinas
COP: peso colombiano
AUD: dolar australiano

"""

# esta funcion es importada mas adelante utilizando la modularizacion, en el archivo main


def moneyChangeAUD(amount, menu, menu2):
    if menu == 5 and menu2 == 1:
        USD = 0.73112
        totalPay = amount * USD
        print('la conversion fue la siguiente',
              amount, 'AUD =>', totalPay, 'USD')

    elif menu == 5 and menu2 == 2:
        EUR = 0.65978
        totalPay = amount * EUR
        print('la conversion fue la siguiente',
              amount, 'AUD =>', totalPay, 'EUR')

    elif menu == 5 and menu2 == 3:
        GBP = 0.5467
        totalPay = amount * GBP
        print('la conversion fue la siguiente',
              amount, 'AUD =>', totalPay, 'GBP')

    elif menu == 5 and menu2 == 4:
        COP = 2784.23
        totalPay = amount * COP
        print('la conversion fue la siguiente',
              amount, 'AUD =>', totalPay, 'COP')
