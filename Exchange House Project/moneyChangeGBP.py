"""
Funcion encargada de converetir librasEs. a dolar EEUU, euros, 
dolar australiano y peso colombiano

USD: dolar EEUU,
EUR: euro,
GBP: libras esterlinas
COP: peso colombiano
AUD: dolar australiano
"""

# esta funcion es importada mas adelante utilizando la modularizacion, en el archivo main


def moneyChangeGBP(amount, menu, menu2):
    if menu == 3 and menu2 == 1:
        USD = 1.33712
        totalPay = amount * USD
        print('la conversion fue la siguiente',
              amount, 'GBP =>', totalPay, 'USD')

    elif menu == 3 and menu2 == 2:
        EUR = 1.20661
        totalPay = amount * EUR
        print('la conversion fue la siguiente',
              amount, 'GBP =>', totalPay, 'EUR')

    elif menu == 3 and menu2 == 4:
        COP = 5091.96
        totalPay = amount * COP
        print('la conversion fue la siguiente',
              amount, 'GBP =>', totalPay, 'COP')

    elif menu == 3 and menu2 == 5:
        AUD = 1.82849
        totalPay = amount * AUD
        print('la conversion fue la siguiente',
              amount, 'GBP =>', totalPay, 'AUD')
