"""
Funcion encargada de converetir euros a dolares EEUU, libras esterlinas, 
dolar australiano y peso colombiano

USD: dolar EEUU,
EUR: euro,
GBP: libras esterlinas
COP: peso colombiano
AUD: dolar australiano
"""

# esta funcion es importada mas adelante utilizando la modularizacion, en el archivo main


def moneyChangeEUR(amount, menu, menu2):
    if menu == 2 and menu2 == 1:
        USD = 1.10805
        totalPay = amount * USD
        print('la conversion fue la siguiente',
              amount, 'EUR =>', totalPay, 'USD')

    elif menu == 2 and menu2 == 3:
        GBP = 0.82859
        totalPay = amount * GBP
        print('la conversion fue la siguiente',
              amount, 'EUR =>', totalPay, 'GBP')

    elif menu == 2 and menu2 == 4:
        COP = 4219.63
        totalPay = amount * COP
        print('la conversion fue la siguiente',
              amount, 'EUR =>', totalPay, 'COP')

    elif menu == 2 and menu2 == 5:
        AUD = 1.51539
        totalPay = amount * AUD
        print('la conversion fue la siguiente',
              amount, 'EUR =>', totalPay, 'AUD')
