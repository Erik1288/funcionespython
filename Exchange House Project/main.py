"""
Archivo main desde el que importamos todas nuestras funciones utilizando la modularidad
"""

from moneyChangeUSD import *
from moneyChangeEUR import *
from moneyChangeGBP import *
from moneyChangeCOP import *
from moneyChangeAUD import *

"""
funcion main, donde capturamos las opciones de cambio de monedas ingresadas por el usuario
mediante un menu de opciones
"""


def main():
    menu = int(input("""
    [1] USD,
    [2] EUR,
    [3] GNB,
    [4] COP,
    [5] AUD
    Eliga con el numero correspondiente el tipo de moneda que desea cambiar? :"""))

    amount = float(input('digite la cantidad que va a cambiar: '))

    menu2 = int(input("""
    [1] USD,
    [2] EUR,
    [3] GNB,
    [4] COP,
    [5] AUD
    Eliga con el numero correspondiente a que tipo de moneda que desea cambiar? :"""))

    # creamos un ciclo infinito para forzar al usuario a que indique valores en un rango entre 1 a 5
    while True:
        if ((menu >= 1 and menu <= 5) and (menu2 >= 1 and menu2 <= 5)):
            if menu == 1:
                moneyChangeUSD(amount, menu, menu2)
            elif menu == 2:
                moneyChangeEUR(amount, menu, menu2)
            elif menu == 3:
                moneyChangeGBP
            elif menu == 4:
                moneyChangeCOP(amount, menu, menu2)
            elif menu == 5:
                moneyChangeAUD(amount, menu, menu2)
        else:
            print('los valores seleccionados deben ser en un rango de 1 y 5')
            print('')
            print('Intentalo nuevamente...')
# --------------------------------------------------------------------------------------
            """utilizamos la recursividad, para llamar a la funcion main nuevamente; 
               cuando el usuario ingrese valores fuera del rango del menu
            """
            main()
        break


if __name__ == "__main__":
    main()
