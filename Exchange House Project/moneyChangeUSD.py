"""
Funcion encargada de converetir dolares a euros, libras esterlinas, 
dolar australiano y peso colombiano

USD: dolar EEUU,
EUR: euro,
GBP: libras esterlinas
COP: peso colombiano
AUD: dolar australiano
"""

# esta funcion es importada mas adelante utilizando la modularizacion, en el archivo main


def moneyChangeUSD(amount, menu, menu2):
    if menu == 1 and menu2 == 2:
        EUR = 0.90236
        totalPay = amount * EUR
        print('la conversion fue la siguiente',
              amount, 'USD =>', totalPay, 'EUR')

    elif menu == 1 and menu2 == 3:
        GBP = 0.74776
        totalPay = amount * GBP
        print('la conversion fue la siguiente',
              amount, 'USD =>', totalPay, 'GBP')

    elif menu == 1 and menu2 == 4:
        COP = 3808.16
        totalPay = amount * COP
        print('la conversion fue la siguiente',
              amount, 'USD =>', totalPay, 'COP')

    elif menu == 1 and menu2 == 4:
        AUD = 1.36748
        totalPay = amount * AUD
        print('la conversion fue la siguiente',
              amount, 'USD =>', totalPay, 'AUD')
