"""
Funcion encargada de converetir moneda Colombiana a dolares EEUU, libras esterlinas, 
dolar australiano y euros
   
USD: dolar EEUU,
EUR: euro,
GBP: libras esterlinas
COP: peso colombiano
AUD: dolar australiano

"""

# esta funcion es importada mas adelante utilizando la modularizacion, en el archivo main


def moneyChangeCOP(amount, menu, menu2):
    if menu == 4 and menu2 == 1:
        USD = 0.00026
        totalPay = amount * USD
        print('la conversion fue la siguiente',
              amount, 'COP =>', totalPay, 'USD')

    elif menu == 4 and menu2 == 2:
        EUR = 0.00024
        totalPay = amount * EUR
        print('la conversion fue la siguiente',
              amount, 'COP =>', totalPay, 'EUR')

    elif menu == 4 and menu2 == 3:
        GBP = 0.0002
        totalPay = amount * GBP
        print('la conversion fue la siguiente',
              amount, 'COP =>', totalPay, 'GBP')

    elif menu == 4 and menu2 == 5:
        AUD = 0.00036
        totalPay = amount * AUD
        print('la conversion fue la siguiente',
              amount, 'COP =>', totalPay, 'AUD')
