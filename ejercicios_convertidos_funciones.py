# funcion que calcula el area de un triangulo

def triangleArea(base, height):
    """La funcion recibe como parametros la base y la altura
    del triangulo"""
    totalTriangleArea = base * height / 2
    return totalTriangleArea


# pedimos los datos por consola, para simular un usuario
triangleBase = float(input('Digite la base del triangulo: '))
triangleHeight = float(input('Digite el area del triangulo: '))

# imprimiendo el resultado
print('El area del triangulo es:', triangleArea(triangleBase, triangleHeight))

# --------------------------//------------------------------------------------

# funcion que determina si un numero es par o no


def validateEvenNumber(number):
    """la funcion recibe como parametro un numero
    y luego valida su residuo, si es 0 entonces el numero es par,
    sino no es cero, entonces el numero es impar"""
    if (number % 2 == 0):
        return("El numero ingresado es par")

    else:
        return("El numero ingresado es impar")


# pedimos al usuario que ingrese el numero a validafr
enteredNumber = int(input("Digite el numero a validar: "))
# imprimimos el resultado
print(validateEvenNumber(enteredNumber))
# ---------------------------------//------------------------------------------

# funcion que calcula la suma de los enteros desde 1 hasta n


def summationValue(positiveInteger):
    """esta funcion recibe como parametro un numero entero
    y retorna la sumatoria desde uno hasta el numero ingresado"""
    if positiveInteger > 0:
        # realizando la sumatoria si el entero es positivo
        summation_Value = positiveInteger * (positiveInteger + 1) / 2
        return (int(summation_Value))


# variable donde se uardara el numero que ingreso el usuario
positive_Integer = int(input("Digite un numero: "))

# imprimiendo el resultado de la sumatoria
print('El resultado de la sumatoria desde 1 hasta ',
      positive_Integer, ' es:', summationValue(positive_Integer))
